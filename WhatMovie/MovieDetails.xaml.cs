﻿using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using WhatMovie.Models;
using WhatMovie.Storage;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace WhatMovie
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MovieDetails : Page
    {
        public Movie citie { get; set; }
        public MovieDetails()
        {
            this.InitializeComponent();
            citie = new Movie();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            citie = (Movie)e.Parameter;
        }

        private void backToList_Click(object sender, RoutedEventArgs e)
        {
            MyFrame.Navigate(typeof(Home));
        }

        private void addToCollection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var db = dbConnection.connectionDB)
                {
                    Movie citieToSave = citie;
                    db.Insert(citieToSave);
                    addToCollection.IsEnabled = false;
                }
            }
            catch (Exception)
            {
                addToCollection.Content = "Can't add citie to collection";
                PopToast();
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            using (var db = dbConnection.connectionDB)
            {
                List<Movie> favoriteMovies = (from p in db.Table<Movie>() select p).ToList();
                foreach (var favMovie in favoriteMovies)
                {
                    if (favMovie.id == citie.id)
                    {
                        addToCollection.Visibility = Visibility.Collapsed;
                    }
                }
            }

           // mdImage.Source = new BitmapImage(new Uri(citie.poster_path, UriKind.Absolute));
            Title.Text = " " + citie.title;
        }

        private void PopToast()
        {
            ToastContent toastContent = CreateDummyToast();
            ToastNotificationManager.CreateToastNotifier()
            .Show(new ToastNotification(toastContent.GetXml()));
        }

        private ToastContent CreateDummyToast()
        {
            return new ToastContent()
            {
                Launch = "action=viewEvent&eventId=1983",
                Scenario = ToastScenario.Default,
                Visual = new ToastVisual()
                {
                    BindingGeneric = new ToastBindingGeneric()
                    {
                        Children =
                        {
                            new AdaptiveText(){Text = $"Good job!"},
                            new AdaptiveText(){Text = $"U added {citie.title} to your collections!"},
                            new AdaptiveText(){Text = $"Go to collection and check this out!"}
                        }
                    }
                }
            };
        }
    }
}
