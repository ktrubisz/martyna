﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WhatMovie.Models;

namespace WhatMovie.Rest
{
    public class MovieApi
    {
    


        public static async Task searchedMoviesToListAsync(ObservableCollection<Movie> citiesList, int page, string query)
        {
            if(query != "")
            {
                var citieData = await getSearchedMoviesAsync(page, query);

                var cities = citieData.results;

                foreach (var citie in cities)
                {
                    string url = "http://www.pngmart.com/files/3/Weather-PNG-HD.png";
                    citie.poster_path = url;
                    // Filter characters that are missing thumbnail images
                    citiesList.Add(citie);
                }
            }
        }

        public static async Task PopularMovieToListAsync(ObservableCollection<Movie> citiesList, int page)
        {
            var citieData = await GetPopularMoviesAsync(page);

            var cities = citieData.results;

            foreach (var citie in cities)
            {
                string url = String.Format("http://www.pngmart.com/files/3/Weather-PNG-HD.png");
                citie.poster_path = url;
                // Filter characters that are missing thumbnail images
                citiesList.Add(citie);
            }
        }

        public async static Task<Movie> GetMovieAsync(int citieId, Movie citie)
        {
            var http = new HttpClient();
            string reqUrl = String.Format("https://www.metaweather.com/api/location/{0}", citieId);
            var response = await http.GetAsync(reqUrl);
            var result = await response.Content.ReadAsStringAsync();
            var sth = JsonConvert.DeserializeObject<RootObject>(result);

            //string url = String.Format("http://www.pngmart.com/files/3/Weather-PNG-HD.png", sth.poster_path);
            //sth.poster_path = url;
            return citie;
        }

        public async static Task<MovieData> GetPopularMoviesAsync(int page)
        {
            string url = "https://www.metaweather.com/api/location/search/?query=a";
            HttpClient http = new HttpClient();
            var response = await http.GetAsync(url);
            var jsonMessage = await response.Content.ReadAsStringAsync();

            var data = JsonConvert.DeserializeObject<List<Movie>>(jsonMessage);
            var citieData = new MovieData()
            {
                results = data.OrderBy(m => m.title).ToList()
            };

            return citieData;
        }


        public async static Task<MovieData> getSearchedMoviesAsync(int page, string query)
        {
            string url = String.Format("https://www.metaweather.com/api/location/search/?query={0}",query);
            HttpClient http = new HttpClient();
            var response = await http.GetAsync(url);
            var jsonMessage = await response.Content.ReadAsStringAsync();

            var data = JsonConvert.DeserializeObject<List<Movie>>(jsonMessage);
            var citieData = new MovieData()
            {
                results = data.OrderBy(m => m.title).ToList()
            };

            return citieData;
        }
    }

}
